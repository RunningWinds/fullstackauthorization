<?php
require_once 'credintails.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once __DIR__ . '/../vendor/autoload.php';
global $mail;
$mail = new PHPMailer();
$mail->isSMTP();
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPAuth = true; 
$mail->Username = AUSER;
$mail->Password = APASS;
$mail->setFrom('from@example.com', 'Fullstackauthorisation');