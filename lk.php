<?php 
if (!empty($_SESSION)){?>
<!DOCTYPE html>
<html lang="ru">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
		integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />
	<title>Личный кабинет</title>
	<style>
		body {
			font-size: 1.5rem;
		}

		p {
			color: red;
			font-weight: bold;
		}

		.edit-btn,
		.save-btn,
		.cancel-btn {
			cursor: pointer;
		}

		.edit-btn,
		.save-btn {
			color: green;
		}
	</style>
</head>

<body>

	<div class="container mt-5">
		<div class="col-7 mx-auto mt-5">

			<p>ID:
				<?php echo $_SESSION["id"]; ?>
			</p>

			<p>Имя: <span>
					<?= $_SESSION["name"]; ?>
				</span>
				<span class="edit-btn">[Изменить]</span>
				<span class="save-btn" hidden data-intention=name>[Сохранить]</span>
				<span class="cancel-btn" hidden>[Отменить]</span>
			</p>

			<p>Фамилия: <span>
					<?php echo $_SESSION["lastname"]; ?>
				</span>
				<span class="edit-btn">[Изменить]</span>
				<span class="save-btn" hidden data-intention=lastname>[Сохранить]</span>
				<span class="cancel-btn" hidden>[Отменить]</span>
			</p>

			<p>E-mail:
				<?php echo $_SESSION["email"]; ?>
			</p>

		</div>
	</div>

	<script>
		let edit_buttons = document.querySelectorAll(".edit-btn");
		let save_buttons = document.querySelectorAll(".save-btn");
		let cancel_buttons = document.querySelectorAll(".cancel-btn");


		for (let i = 0; i < edit_buttons.length; i++) {
			let inputValue = edit_buttons[i].previousElementSibling.innerText;


			edit_buttons[i].addEventListener("click", () => {

				edit_buttons[i].previousElementSibling.innerHTML = `<input type="text" value="${inputValue}">`;
				save_buttons[i].hidden = false;
				cancel_buttons[i].hidden = false;
				edit_buttons[i].hidden = true;

			});
			cancel_buttons[i].addEventListener("click", () => { 

				edit_buttons[i].previousElementSibling.innerHTML = '<span>'+inputValue+'</span>';
				save_buttons[i].hidden = true;
				cancel_buttons[i].hidden = true;
				edit_buttons[i].hidden = false;

			});

			save_buttons[i].addEventListener("click", async () => {
				let key = save_buttons[i].dataset.intention;
				val = edit_buttons[i].previousElementSibling.firstChild.value;
				let formData = new FormData();
				formData.append("key",key);
				formData.append("value", val);
				
				let response = await fetch("chgUser", {
					method: "POST",
					body:formData
				});
				let result = await response.text();

				inputValue=val;
				edit_buttons[i].previousElementSibling.innerHTML = '<span>'+inputValue+'</span>';
				save_buttons[i].hidden = true;
				cancel_buttons[i].hidden = true;
				edit_buttons[i].hidden = false;
			});
		}
	</script>
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF"
		crossorigin="anonymous"></script>
</body>

</html>
<?php
}