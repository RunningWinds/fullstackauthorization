<?php
class User {
  private $name;
  private $lastname;
  private $email;
  private $id;

  function __construct($id, $name, $lastname, $email)
  {
    $this->id = $id;
    $this->name = $name;
    $this->lastname = $lastname;
    $this->email = $email;
  }

  function getId() {
    return $this->id;
  }
  function getName() {
    return $this->name;
  }
  function getLastname() {
    return $this->lastname;
  }
  function getEmail() {
    return $this->email;
  }

  static function regUser($name, $lastname, $email, $pass) {
    require_once('Mail.php');
    global $mysqli;
    $email = mb_strtolower(trim($email));
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $pass = trim($pass);
        $pass = password_hash($pass, PASSWORD_DEFAULT);

        $result = $mysqli->query("SELECT * FROM `users` WHERE `email`='$email'");
        if ($result->num_rows != 0) {
          return "exist";
        } else {
        $hash = md5($name . time());
        $mail->Subject = 'Account verification';
        $mail->addAddress($email, 'client');
        $mail->MsgHTML('
            <html>
            <head>
            <title>Verify your email address</title>
            </head>
            <body>
            <p>To verify your email, please go to <a href="http://'. $_SERVER['SERVER_NAME'] .'/confUser?hash=' . $hash . '"> link</a></p>
            </body>
            </html>
        ');
      
        if ($mail->Send()){ 
          $mysqli->query("INSERT INTO `users`(`name`, `lastname`, `email`, `pass`, `hash`, `confirmed`) VALUES ('$name', '$lastname', '$email', '$pass', '$hash', 1)");
         return "ok";
        }
        else {
            return "emailerror";
        }
      }
    } 
    else {
      return "bademail";
    }
  }  

  static function authUser($email, $pass) {
    global $mysqli;
    $email = trim(mb_strtolower($email));
    $pass = trim($pass);

    $result = $mysqli->query("SELECT * FROM `users` WHERE `email`='$email'");
    $result = $result->fetch_assoc();

    if (password_verify($pass, $result["pass"])) {
    if ($result["confirmed"]==0){
      $_SESSION["id"] = $result["id"];
      $_SESSION["name"] = $result["name"];
      $_SESSION["lastname"] = $result["lastname"];
      $_SESSION["email"] = $result["email"];
      return "ok";
    }
    else {
      return "not_confirmed";
    }
    } else {
     return "user_not_found";
    }
  }

  static function chgUser($value,$key){
    global $mysqli;
    $id=$_SESSION["id"];
    $mysqli->query("UPDATE `users` SET `$key`='$value' WHERE id=$id;");
    $_SESSION[$key] = $value;
  }
}