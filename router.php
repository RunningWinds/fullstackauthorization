<?php
session_start();
$url = explode("/", $_SERVER['REQUEST_URI']);
require_once("./php/db.php");
require_once("./php/User.php");

if ($url[1] == "auth") {
  $content = file_get_contents("./auth.html");
} else if ($url[1] == "reg") {
  $content = file_get_contents("./reg.html");
} else if (str_contains($url[1],"confUser")) {
  require_once("./php/confirmed.php");
} else if ($url[1] == "lk") {
  require_once('./lk.php');
} else if ($url[1]=="authUser") {
  echo User::authUser($_POST["email"], $_POST["pass"]);
} else if ($url[1] == "regUser") {
  echo User::regUser($_POST["name"], $_POST["lastname"], $_POST["email"], $_POST["pass"]);
} else if ($url[1] == "chgUser"){
  echo User::chgUser($_POST["value"],$_POST["key"]);
}
else {
  $content = file_get_contents("./index.html");
}

if (!empty($content)) require_once("./template.php");